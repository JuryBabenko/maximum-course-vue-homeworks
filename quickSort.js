function quickSort(arr) {
  if (arr.length < 2) {
    return arr;
  }

  const pivotIndex = Math.floor(arr.length / 2);
  const pivot = arr[pivotIndex];

  const less = arr.filter((val, index) => {
    const isPivot = index === pivotIndex;
    return !isPivot && (val <= pivot);
  });

  const greater = arr.filter(val => val > pivot);

  return [...quickSort(less), pivot, ...quickSort(greater)];
}

const arr = [1, 5, 3, 7, 8, 9, 4, 2, 6, 0];
console.log(quickSort(arr));
