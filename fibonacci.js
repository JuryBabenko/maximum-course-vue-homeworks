function fibRecursive(n) {
  return n <= 1
    ? n
    : fibRecursive(n - 1) + fibRecursive(n - 2);
}

function fibOptimal(n) {
  let a = 1;
  let b = 1;

  for (let i = 2; i < n; i++) {
    let c = a + b;
    a = b;
    b = c;
  }

  return b;
}
console.time('Recursive time');
console.log('Recursive', fibRecursive(40));
console.timeEnd('Recursive time');

console.time('Optimal time');
console.log('Optimal', fibOptimal(40));
console.timeEnd('Optimal time');
